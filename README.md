# README #

## Computer scripts to reproduce simulation in Costilla et al 2018 ##
This repository contains the R and C++ source files to reproduce results presented in Table 2 (Section 3.4 Model validation using simulated data) 
of the paper. A brief description of these source file are found below.

**R scripts**

			(1) pomttr.sim.r. 
			Simulates data, estimates the model using a Metropolis-Hastings sampler, and relabels MCMC chains.

			(2) functions.pomttr.r
			Functions to perform all above taks, including compiling C++ scripts below.


**C++ scripts**

			(3) theta.pomttr.cpp
			Calculates cell probabilities given parameters mu, alpha, beta, and gamma.

			(4) Zrc_tt.cpp
			Numerator for z_ig in logs. Likelihood as a function of theta, pi, and data.
  
  
  
### Running instructions ###

Scripts run in Linux and Windows (tested in 64 versions). To run the simulation please:

+ Download and uncompress repository using Downloads section (left panel).
+ Run "pomttr.sim.r" within R. Note that you need to access to several R libraries, including R tools to compile C++ code.
+ Table 2 contents will be saved as a csv file. 

Programs take about an hour to run using R 3.3.3 in a Xeon E5-2680 2.50GHz CPU. Depending on your computer specifications
this time might vary. Running time includes simulating the data (n=1000, p=15, q=5, R=3), estimating the model using 3 MCMC chains and relabeling these chains. 
In addition to that, traceplots for the original and relabelled chains are also produced and the R session saved. 

   
   
### References ###
Costilla, Liu, Arnold, Fernandez (2018). A Bayesian model-based approach to estimate latent groups in longitudinal ordinal data (submitted).

### Comments/questions to ###
Roy Costilla

PhD Statistics

Institute for Molecular Biosciences. Univerity of Queensland

r.costilla@imb.uq.edu.au

https://www.researchgate.net/profile/Roy_Costilla

https://twitter.com/CmRoycostilla

